import {extractTextFromFile} from './extractTextFromFile.js';
import {stats} from './stats.js';

export {
    extractTextFromFile,
    stats
};
