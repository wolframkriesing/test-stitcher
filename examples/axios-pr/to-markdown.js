import {extractTextFromFile} from '../../src/extractTextFromFile.js';

const capitalize = s => s[0].toUpperCase() + s.slice(1);

export const toMarkdownFromFile = async (filename) => {
  const suite = (await extractTextFromFile(filename)).suites[0].suites[0];
  return [
    capitalize(suite.name),
    suite.suites[0].name,
    ...suite.suites[0].tests.map(t => '- ' + t.name),
    '- ' + suite.suites[0].suites[0].name,
    ...suite.suites[0].suites[0].tests.map(t => '    - ' + t.name),
  ].join('\n');
};
